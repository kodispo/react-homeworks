import React, {Component} from 'react';
import PropTypes from 'prop-types';
import hashtagsList from '../../data/hashtags.json';
import authorsList from '../../data/authors.json';
import Select from 'react-select';
import {getBase64} from "../../utils/base64";
import faker from 'faker';


class NewsForm extends Component {
    state = {
        newsItem: {
            title: '',
            excerpt: '',
            description: '',
            photo: '',
            hashtags: [],
            author: null,
        },
        errors: {
            title: null,
            description: null,
            photo: null,
            hashtags: null,
            author: null,
        }
    };

    handleChangeText = (e) => {
        this.setState({
            newsItem: {
                ...this.state.newsItem,
                [e.target.name]: e.target.value,
            }
        })
    };

    handleChangePhoto = (e) => {
        getBase64(e.currentTarget.files[0], (base64) => {
            this.setState({
                newsItem: {
                    ...this.state.newsItem,
                    photo: base64,
                }
            });
        });
    };

    handleSelectChange = (options, field) => {
        this.setState({
            newsItem: {
                ...this.state.newsItem,
                [field.name]: options,
            }
        })
    };

    handleSubmit = (e) => {
        e.preventDefault();
        const {newsItem, errors} = this.state;
        const newErrors = {};
        let submit = true;
        for (const [key] of Object.entries(errors)) {
            const hasError = !newsItem[key] || (Array.isArray(newsItem[key]) && newsItem[key].length === 0);
            newErrors[key] = hasError;
            if (hasError) submit = false;
        }
        this.setState({errors: newErrors});
        if (submit) {
            this.props.onAddNewsItem({
                id: faker.datatype.uuid(),
                ...this.state.newsItem
            });
        }
    };

    getRandomHashTags() {
        const randomTags = hashtagsList.filter(() => Math.random() < 0.5);
        if (!randomTags.length) return this.getRandomHashTags();
        return randomTags;
    }

    handleSeederClick = () => {
        this.setState({
            newsItem: {
                title: faker.lorem.sentence(3),
                excerpt: faker.lorem.sentence(10),
                description: faker.lorem.sentence(100),
                photo: `https://picsum.photos/400/300?v=${new Date().getTime()}`,
                hashtags: this.getRandomHashTags(),
                author: authorsList[Math.floor(Math.random() * authorsList.length)],
            }
        })
    }

    render() {
        const {title, excerpt, description, hashtags, author} = this.state.newsItem;
        const {errors} = this.state;

        return (
            <form onSubmit={this.handleSubmit} className="NewsForm">
                <div className="mb-3">
                    <label className="d-block">
                        <span>Title</span>
                        <input
                            type="text"
                            className="form-control"
                            name="title"
                            value={title}
                            onChange={this.handleChangeText}
                        />
                        {errors.title && (<span className="text-danger error">The field is required</span>)}
                    </label>
                </div>
                <div className="mb-3">
                    <label className="d-block">
                        <span>Excerpt</span>
                        <textarea
                            className="form-control"
                            name="excerpt"
                            rows="2"
                            value={excerpt}
                            onChange={this.handleChangeText}
                        ></textarea>
                    </label>
                </div>
                <div className="mb-3">
                    <label className="d-block">
                        <span>Description</span>
                        <textarea
                            className="form-control"
                            name="description"
                            rows="4"
                            value={description}
                            onChange={this.handleChangeText}
                        ></textarea>
                        {errors.description && (<span className="text-danger error">The field is required</span>)}
                    </label>
                </div>
                <div className="mb-3">
                    <label className="d-block">
                        <span>Photo</span>
                        <input
                            type="file"
                            className="form-control"
                            name="photo"
                            accept=".jpg,.jpeg,.png"
                            onChange={this.handleChangePhoto}
                        />
                        {errors.photo && (<span className="text-danger error">The field is required</span>)}
                    </label>
                </div>
                <div className="mb-3">
                    <div>Hash tags</div>
                    <Select
                        name="hashtags"
                        value={hashtags}
                        isMulti
                        onChange={this.handleSelectChange}
                        options={hashtagsList}
                    />
                    {errors.hashtags && (<span className="text-danger error">The field is required</span>)}
                </div>
                <div className="mb-3">
                    <div>Author</div>
                    <Select
                        name="author"
                        value={author}
                        onChange={this.handleSelectChange}
                        options={authorsList}
                    />
                    {errors.author && (<span className="text-danger error">The field is required</span>)}
                </div>
                <div className="mt-4">
                    <button type="submit" className="btn btn-outline-primary">Add news item</button>
                    <button
                        type="button"
                        className="btn btn-outline-success mx-3"
                        onClick={this.handleSeederClick}
                    >Populate fields</button>
                </div>
            </form>
        );
    }
}

NewsForm.propTypes = {
    onAddNewsItem: PropTypes.func,
};

export default NewsForm;
