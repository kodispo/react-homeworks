import React, {Component} from 'react';
import PropTypes from 'prop-types';

class NewsItem extends Component {
    render() {
        const {item, onRemoveNews} = this.props;
        const {id, title, excerpt, description, photo, author} = item;
        const hashtags = item.hashtags.map(item => item.value).join(', ');
        return (
            <div className="news-item">
                <div className="news-item-photo mb-3">
                    <img src={photo} alt={title}/>
                </div>
                <h3>{title}</h3>
                <p><strong>Tags: {hashtags}</strong></p>
                <h5>{excerpt}</h5>
                <p>{description}</p>
                <p><strong>Author: {author.value}</strong></p>
                <button
                    type="button"
                    onClick={() => onRemoveNews(id)}
                    className="btn btn-outline-danger btn-sm"
                >Drop news</button>
            </div>
        );
    }
}

NewsItem.propTypes = {
    item: PropTypes.shape({
        id: PropTypes.string,
        title: PropTypes.string,
        excerpt: PropTypes.string,
        description: PropTypes.string,
        photo: PropTypes.string,
        hashtags: PropTypes.arrayOf(
            PropTypes.shape({
                label: PropTypes.string,
                value: PropTypes.string,
            })
        ),
        author: PropTypes.shape({
            label: PropTypes.string,
            value: PropTypes.string,
        }),
    }),
    onRemoveNews: PropTypes.func
};

export default NewsItem;
