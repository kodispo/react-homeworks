import React, {Component} from 'react';
import NewsForm from "./NewsForm";
import NewsList from "./NewsList";

export class NewsPage extends Component {
    constructor(props) {
        super(props);
        let storageNews = localStorage.getItem('newsState');
        storageNews = storageNews ? JSON.parse(storageNews) : null;
        this.state = storageNews ? storageNews : {
            news: [],
            isFormOpen: false
        }
    }

    addNewsItem = (newsItemObj) => {
        this.setState({
            news: [
                newsItemObj,
                ...this.state.news,
            ]
        }, this.updateLocalStorage);
    };

    removeNews = (id) => {
        this.setState({
            news: this.state.news.filter((item) => item.id !== id),
        }, this.updateLocalStorage);
    };

    handleCreateNewsClick = () => {
        this.setState({
            isFormOpen: !this.state.isFormOpen,
        }, this.updateLocalStorage);
    }

    updateLocalStorage() {
        localStorage.setItem('newsState', JSON.stringify(this.state));
    }

    render() {
        const {news, isFormOpen} = this.state;
        return (
            <div className="NewsPage container py-5">
                <button type="button" className="btn btn-outline-primary mb-3" onClick={this.handleCreateNewsClick}>
                    {isFormOpen ? 'Hide' : 'Show'} news form
                </button>
                {isFormOpen && (<NewsForm onAddNewsItem={this.addNewsItem}/>)}
                <NewsList news={news} onRemoveNews={this.removeNews}/>
            </div>
        );
    }
}