import React, {Component} from 'react';

class Tree extends Component {
    renderTree(item) {
        return (
            <li key={item.id} className={this.hasChildren(item) ? 'has-children' : undefined}>
                <div onClick={this.hasChildren(item) ? this.handleClick : undefined}>
                    <i className={this.getIconClass(item)}>icon</i>
                    <span>{item.name}</span>
                </div>
                {this.hasChildren(item) && (
                    <ul>{item.children.map(item => this.renderTree(item))}</ul>
                )}
            </li>
        )
    }

    handleClick(e) {
        const parent = e.target.closest('li');
        parent.classList.toggle('expanded');
        if (!parent.classList.contains('expanded')) {
            parent.querySelectorAll('.expanded').forEach(el => {
                el.classList.remove('expanded');
            });
        }
    }

    hasChildren(item) {
        return item.children !== null && item.children.length > 0;
    }

    getIconClass(item) {
        if (item.type === 'file') {
            switch (item.name.split('.').pop()) {
                case 'pdf':  return 'icon-file-pdf';
                case 'xlsx': return 'icon-file-excel';
                case 'docx': return 'icon-doc';
            }
        } else {
            return this.hasChildren(item) ? 'icon-folder' : 'icon-folder-open-empty';
        }
    }

    render() {
        const {data} = this.props;
        return data && (
            <ul className="tree">
                {data.map(item => this.renderTree(item))}
            </ul>
        );
    }
}

export default Tree;