import React, {Component} from 'react';
import Headline from "../Headline";
import Tree from "./components/Tree";
import directories from "../../data/directories.json";

class Maximum extends Component {
    render() {
        return (
            <>
                <Headline text="Maximum"/>
                <Tree data={directories}/>
            </>
        );
    }
}

export default Maximum;