import React, {Component} from "react";
import {NavLink} from "react-router-dom";

export default class Nav extends Component {
    render() {
        return (
            <nav>
                <ul className="nav justify-content-center">
                    <li className="nav-item">
                        <NavLink to="/" className="nav-link">Minimum</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink to="/optimum" className="nav-link">Optimum</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink to="/maximum" className="nav-link">Maximum</NavLink>
                    </li>
                </ul>
            </nav>
        );
    }
}
