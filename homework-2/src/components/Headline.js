import React, {Component} from "react";

export default class Headline extends Component {
    render() {
        return (
            <h1 className="text-center mb-5">{this.props.text}</h1>
        );
    }
}
