import React, {Component} from "react";
import Headline from "../Headline";
import news from "../../data/news.json";
import {LazyLoadImage} from "react-lazy-load-image-component";
import moment from "moment";

export default class Minimum extends Component {
    renderNews(newsObj) {
        return newsObj.map((item) => {
            const {id, title, content, dateCreated, categories, link, photo, author, isSpecial} = item;

            const mewsItemInnerContent = (
                <>
                    <div className="news-item-img">
                        {photo && (<LazyLoadImage src={photo} effect="blur"/>)}
                    </div>
                    <div className="news-item-content">
                        <h3 className="news-item-title">{title}</h3>
                        <div className="news-item-head">
                            <p className="news-item-author">{author}</p>
                            <p className="news-item-date">{moment(dateCreated).format('ll')}</p>
                        </div>
                        {categories && (
                            <ul className="news-item-cats">
                                {this.renderCategories(categories)}
                            </ul>
                        )}
                        <div className="news-item-text" dangerouslySetInnerHTML={{__html: content}}></div>
                    </div>
                </>
            )

            return (
                <div key={id} className={'news-item-col col-12' + (isSpecial ? ' featured' : '')}>
                    {isSpecial && (<span className="badge bg-primary special-badge">Special</span>)}
                    {link ? (
                        <a className="news-item" href={link}>
                            {mewsItemInnerContent}
                        </a>
                    ) : (
                        <div className="news-item">
                            {mewsItemInnerContent}
                        </div>
                    )}
                </div>
            )
        });
    }

    renderCategories(categoriesArr) {
        return categoriesArr.map((item) => {
            const {id, name} = item;
            return (
                <li key={id}>
                    <span className="badge bg-warning text-dark">{name}</span>
                </li>
            )
        });
    }

    render() {
        return (
            <>
                <Headline text="Minimum"/>
                <div className="news row">
                    {this.renderNews(news)}
                </div>
            </>
        );
    }
}
