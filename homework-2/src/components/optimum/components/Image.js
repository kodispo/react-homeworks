import React, {Component} from 'react';
import {LazyLoadImage} from "react-lazy-load-image-component";

class Image extends Component {
    render() {
        const {photo} = this.props;
        return (
            <div className="news-item-img">
                {photo && (<LazyLoadImage src={photo} effect="blur"/>)}
            </div>
        );
    }
}

export default Image;