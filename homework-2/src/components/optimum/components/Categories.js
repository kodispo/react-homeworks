import React, {Component} from 'react';

class Categories extends Component {
    renderCategories(categoriesArr) {
        return categoriesArr.map((item) => {
            const {id, name} = item;
            return (
                <li key={id}>
                    <span className="badge bg-warning text-dark">{name}</span>
                </li>
            )
        });
    }

    render() {
        const {categories} = this.props;
        return categories && (
            <ul className="news-item-cats">
                {this.renderCategories(categories)}
            </ul>
        );
    }
}

export default Categories;