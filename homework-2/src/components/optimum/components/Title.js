import React, {Component} from 'react';

class Title extends Component {
    render() {
        const {title} = this.props;
        return (
            <h3 className="news-item-title">{title}</h3>
        );
    }
}

export default Title;