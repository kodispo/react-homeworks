import React, {Component} from 'react';

class Content extends Component {
    render() {
        const {content} = this.props;
        return (
            <div className="news-item-text" dangerouslySetInnerHTML={{__html: content}}></div>
        );
    }
}

export default Content;