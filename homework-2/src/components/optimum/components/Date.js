import React, {Component} from 'react';
import moment from "moment";

class Date extends Component {
    render() {
        const {dateCreated} = this.props;
        return (
            <p className="news-item-date">{moment(dateCreated).format('ll')}</p>
        );
    }
}

export default Date;