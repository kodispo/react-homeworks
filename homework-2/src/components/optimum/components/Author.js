import React, {Component} from 'react';

class Author extends Component {
    render() {
        const {author} = this.props;
        return (
            <p className="news-item-author">{author}</p>
        );
    }
}

export default Author;