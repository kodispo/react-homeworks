import React, {Component} from 'react';
import Image from "./Image";
import Title from "./Title";
import Author from "./Author";
import Date from "./Date";
import Categories from "./Categories";
import Content from "./Content";

class NewsItem extends Component {
    renderNewsItem(itemObject) {
        const {title, content, dateCreated, categories, photo, author} = itemObject;
        return (
            <>
                <Image photo={photo} />
                <div className="news-item-content">
                    <Title title={title} />
                    <div className="news-item-head">
                        <Author author={author} />
                        <Date dateCreated={dateCreated} />
                    </div>
                    <Categories categories={categories} />
                    <Content content={content} />
                </div>
            </>
        )
    }

    render() {
        const {itemObject} = this.props;
        const {link, isSpecial} = itemObject;
        return (
            <div className={'news-item-col col-12' + (isSpecial ? ' featured' : '')}>
                {isSpecial && (<span className="badge bg-primary special-badge">Special</span>)}
                {link ? (
                    <a className="news-item" href={link}>
                        {this.renderNewsItem(itemObject)}
                    </a>
                ) : (
                    <div className="news-item">
                        {this.renderNewsItem(itemObject)}
                    </div>
                )}
            </div>
        );
    }
}

export default NewsItem;