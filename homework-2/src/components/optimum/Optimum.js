import React, {Component} from "react";
import Headline from "../Headline";
import news from "../../data/news.json";
import NewsItem from "./components/NewsItem";

export default class Optimum extends Component {
    render() {
        return (
            <>
                <Headline text="Optimum"/>
                <div className="news row">
                    {news && news.map((itemObject) => (
                        <NewsItem key={itemObject.id} itemObject={itemObject} />
                    ))}
                </div>
            </>
        );
    }
}
