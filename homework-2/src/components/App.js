import React, {Component} from "react";
import {Routes, Route} from "react-router-dom";
import Nav from "./Nav";
import Minimum from "./minimum/Minimum";
import Optimum from "./optimum/Optimum";
import Maximum from "./maximum/Maximum";

export default class App extends Component {
    render() {
        return (
            <div className="App">
                <Nav />
                <main className="container">
                    <Routes>
                        <Route path="/" element={<Minimum />} />
                        <Route path="optimum" element={<Optimum />} />
                        <Route path="maximum" element={<Maximum />} />
                    </Routes>
                </main>
            </div>
        );
    }
}
