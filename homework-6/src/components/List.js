import React, {Component} from 'react';
import axios from "axios";

const STARSHIPS_API_URL = 'https://www.swapi.tech/api/starships/?page=1&limit=100';

class List extends Component {
    state = {
        status: 'initial', // loading|success|error
        error: null,
        list: null,
    }

    getStarships = () => {
        this.setState({
            status: 'loading',
            error: null,
            list: null,
        })

        axios.get(STARSHIPS_API_URL)
            .then((response) => {
                this.setState({
                    status: 'success',
                    error: null,
                    list: response.data.results,
                })
                return response;
            })
            .catch((error) => {
                console.error(error);
                this.setState({
                    status: 'error',
                    error: 'Failed to load starships. Please try again later',
                    list: null,
                })
            });
    }

    render() {
        const {status, error, list} = this.state;
        return (
            <div className="List container py-5">
                <h1 className="mb-5 text-center">Starships</h1>
                {status === 'loading' && (<p className="text-primary text-center">Loading starships...</p>)}
                {status === 'error' && (<p className="text-danger text-center">{error}</p>)}
                {status === 'success' &&
                    list.map(({uid, name, url}) => (
                        <ul key={uid} className="list-group mb-3">
                            <li className="list-group-item"><strong>UID: </strong> {uid}</li>
                            <li className="list-group-item"><strong>Name: </strong> {name}</li>
                            <li className="list-group-item"><strong>URL: </strong> {url}</li>
                        </ul>
                    ))
                }
            </div>
        );
    }

    componentDidMount() {
        this.getStarships();
    }
}

export default List;