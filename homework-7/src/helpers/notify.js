import Noty from "noty";

export function notify(text, type = 'error') {
    new Noty({
        text: text,
        type: type,
        timeout: 2000,
    }).show();
}