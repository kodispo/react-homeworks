import React, {Component, createRef} from 'react';
import {notify} from "../helpers/notify";
import gsap from "gsap";

class Form extends Component {
    headlineEl = createRef();
    titleRowEl = createRef();
    textRowEl = createRef();
    tagsRowEl = createRef();
    submitEl = createRef();
    titleFieldEl = createRef();
    textFieldEl = createRef();

    handleSubmit = (e) => {
        e.preventDefault();

        // fields
        const titleFieldEl = this.titleFieldEl.current;
        const textFieldEl = this.textFieldEl.current;
        const tagsRowEl = this.tagsRowEl.current;

        // values
        const title = titleFieldEl.value;
        const text = textFieldEl.value;
        const tagCheckboxEls = tagsRowEl.querySelectorAll('[type="checkbox"]');
        const hashtags = Array.from(tagCheckboxEls).filter(el => el.checked).map(el => el.value).join(', ');
        const valuesObj = {title, text, hashtags};

        if (this.isValid(valuesObj)) {
            console.table(valuesObj);
            notify('You\'re done! Check the dev console.', 'success');
        }
    }

    isValid(valuesObj) {
        let validation = true;
        if (!valuesObj.title) {
            notify('Title is required');
            validation = false;
        }
        if (!valuesObj.text) {
            notify('Text is required');
            validation = false;
        }
        if (!valuesObj.hashtags.length) {
            notify('Choose at least one hashtag');
            validation = false;
        }
        return validation;
    }

    render() {
        return (
            <div className="Form container py-5">
                <form onSubmit={this.handleSubmit}>
                    <h3 ref={this.headlineEl} className="mb-3 text-center">Form</h3>
                    <div ref={this.titleRowEl} className="mb-3">
                        <label className="form-label d-block">
                            <span>Title</span>
                            <input ref={this.titleFieldEl} type="text" className="form-control" />
                        </label>
                    </div>
                    <div ref={this.textRowEl} className="mb-3">
                        <label className="form-label d-block">
                            <span>Text</span>
                            <textarea ref={this.textFieldEl} className="form-control" rows="4"></textarea>
                        </label>
                    </div>
                    <div ref={this.tagsRowEl}>
                        <span className="d-block">Hashtags</span>
                        <div className="btn-group mb-3 align-items-center" role="group">
                            <input type="checkbox"
                                   className="btn-check"
                                   autoComplete="off"
                                   id="hashtag_1"
                                   value="hashtag_1"
                            />
                            <label className="btn btn-outline-primary" htmlFor="hashtag_1">#hashtag_1</label>

                            <input type="checkbox"
                                   className="btn-check"
                                   autoComplete="off"
                                   id="hashtag_2"
                                   value="hashtag_2"
                            />
                            <label className="btn btn-outline-primary" htmlFor="hashtag_2">#hashtag_2</label>

                            <input type="checkbox"
                                   className="btn-check"
                                   autoComplete="off"
                                   id="hashtag_3"
                                   value="hashtag_3"
                            />
                            <label className="btn btn-outline-primary" htmlFor="hashtag_3">#hashtag_3</label>
                        </div>
                    </div>
                    <button ref={this.submitEl} type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>
        );
    }

    componentDidMount() {
        const start = () => ({opacity: 0, scale: .7});
        const end = () => ({opacity: 1, scale: 1, ease: 'power4.out', duration: .6});
        const delta = '-=.3';

        gsap.timeline()
            .add(gsap.fromTo(this.headlineEl.current, start(), end()), .5)
            .add(gsap.fromTo(this.titleRowEl.current, start(), end()), delta)
            .add(gsap.fromTo(this.textRowEl.current, start(), end()), delta)
            .add(gsap.fromTo(this.tagsRowEl.current, start(), end()), delta)
            .add(gsap.fromTo(this.submitEl.current, start(), end()), delta);
    }
}

export default Form;
