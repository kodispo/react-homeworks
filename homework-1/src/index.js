import React from 'react';
import ReactDOM from 'react-dom';

// styles
import 'bootstrap/scss/bootstrap.scss';
import "noty/src/noty.scss";
import "noty/src/themes/mint.scss";
import './styles/index.scss';

import App from './components/App';

ReactDOM.render(<App />, document.getElementById('root'));