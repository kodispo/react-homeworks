function Info({weatherInfo}) {
    let listItems;

    if (weatherInfo) {
        listItems = weatherInfo.map((item, index) =>
            <li key={index} className="list-group-item">
                <span>{item.key}:</span> {item.value}
            </li>
        );
    }

    return (
        <ul className="weather-info list-group list-group-flush">
            {listItems || ''}
        </ul>
    );
}

export default Info;