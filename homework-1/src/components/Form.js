import { useFormik } from 'formik';
import { notify } from "../utils/notify";
import axios from "axios";
import {Fragment} from "react";
import Geo from "./Geo";

function Form({getWeatherInfo}) {
    const formik = useFormik({
        validateOnChange: false,
        validateOnBlur: false,
        initialValues: {
            city: '',
        },
        validate,
        onSubmit: async (values) => {
            getWeatherInfo(await getWeatherInfoArr(values.city));
        },
    });

    async function getGeoLocation(city) {
        getWeatherInfo(await getWeatherInfoArr(city));
    }

    return (
        <Fragment>
            <form onSubmit={formik.handleSubmit}>
                <input
                    id="city"
                    className="form-control"
                    name="city"
                    type="text"
                    placeholder="City (e.g. New York)"
                    autoComplete="off"
                    onChange={formik.handleChange}
                    value={formik.values.city}
                />
                <button className="btn btn-outline-warning" type="submit">Checkout weather</button>
            </form>
            <Geo getGeoLocation={getGeoLocation} />
        </Fragment>
    );
}

function validate(values) {
    const errors = {};
    if (!values.city) {
        notify('You forgot to type the city');
        this.setSubmitting(false);
    }
    return errors;
}

async function getWeatherData(city) {
    let weatherData;

    await axios.get('https://api.openweathermap.org/data/2.5/weather', {
        params: {
            q: city,
            appid: process.env.REACT_APP_WEATHER_API_KEY,
            units: 'metric',
        }
    })
        .then(function(response) {
            if (response.status === 200) {
                weatherData = response.data;
            }
        })
        .catch(function(error) {
            notify('Nah, are you sure such city exists?');
            console.log(error);
        });

    return weatherData;
}

async function getWeatherInfoArr(city) {
    let weatherData = await getWeatherData(city);
    return [
        {
            key: 'City',
            value: `${weatherData.name}, ${weatherData.sys.country}`,
        },
        {
            key: 'Weather',
            value: weatherData.weather[0].description,
        },
        {
            key: 'Temperature',
            value: `${Math.round(weatherData.main.temp)}°C`,
        },
        {
            key: 'Humidity',
            value: `${weatherData.main.humidity}%`,
        },
        {
            key: 'Pressure',
            value: `${weatherData.main.pressure}hPa`,
        },
        {
            key: 'Wind',
            value: `${weatherData.wind.speed}m/s`,
        },
    ]
}

export default Form;