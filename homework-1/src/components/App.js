import { useState } from 'react';
import Form from './Form';
import Info from "./Info";

function App() {
    const [weatherInfo, setWeatherInfo] = useState(false);

    function getWeatherInfo(weatherInfo) {
        setWeatherInfo(weatherInfo);
    }

    return (
        <div className="weather-widget">
            <Form getWeatherInfo={getWeatherInfo} />
            <Info weatherInfo={weatherInfo} />
        </div>
    );
}

export default App;
