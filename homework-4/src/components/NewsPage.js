import React, {Component} from 'react';
import NewsFilters from "./NewsFilters";
import NewsList from "./NewsList";
import news from "../data/news.json";

class NewsPage extends Component {
    constructor(props) {
        super(props);

        let storageFilters = sessionStorage.getItem('filters');
        storageFilters = storageFilters ? JSON.parse(storageFilters) : null;
        this.state = {
            filters: {
                photo: storageFilters ? storageFilters.photo : false,
                link: storageFilters ? storageFilters.link : false,
                isSpecial: storageFilters ? storageFilters.isSpecial : false,
                categories: storageFilters ? storageFilters.categories : false,
            },
        }
    }

    getCategories() {
        const categories = news.map((obj) => obj.categories).flat().map((obj) => obj.name);
        return [...new Set(categories)];
    }

    setFilter = (filter) => {
        this.setState({
            filters: {
                ...this.state.filters,
                ...filter,
            }
        }, () => {
            sessionStorage.setItem('filters', JSON.stringify(this.state.filters));
        });
    }

    render() {
        let filteredNews = news;

        for (const [key, value] of Object.entries(this.state.filters)) {
            if (!value || (Array.isArray(value) && value.length === 0)) continue;
            if (key === 'categories') {
                filteredNews = filteredNews.filter((newsItem) => {
                    const itemCatsArr = newsItem[key].map((obj) => obj.name);
                    return value.every((cat) => itemCatsArr.includes(cat));
                });
            } else {
                filteredNews = filteredNews.filter((newsItem) => newsItem[key]);
            }
        }

        return filteredNews && (
            <div className="NewsPage container py-5">
                <NewsFilters setFilter={this.setFilter}
                             filters={this.state.filters}
                             categories={this.getCategories()}
                />
                <NewsList news={filteredNews}/>
            </div>
        );
    }
}

export default NewsPage;