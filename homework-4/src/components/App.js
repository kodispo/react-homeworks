import React, {Component} from "react";
import NewsPage from "./NewsPage";

export default class App extends Component {
    render() {
        return (
            <div className="App">
                <NewsPage />
            </div>
        );
    }
}
