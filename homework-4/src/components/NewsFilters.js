import React, {Component} from 'react';
import PropTypes from "prop-types";
import Select from 'react-select';

class NewsFilters extends Component {
    handleCheckboxChange = (e) => {
        this.props.setFilter({
            [e.target.value]: e.target.checked,
        });
    }

    handleSelectChange = (selectedOptions) => {
        this.props.setFilter({
            categories: this.convertOptionsToCats(selectedOptions),
        });
    };

    convertCatsToOptions(categories) {
        return categories && categories.map((item) => ({ value: item, label: item }));
    }

    convertOptionsToCats(options) {
        return options && options.map((item) => item.value);
    }

    render() {
        const {filters, categories} = this.props;
        const value = this.convertCatsToOptions(filters.categories);

        return (
            <div className="NewsFilters mb-5">
                <div className="mb-3">
                    <div><strong>General filters: </strong></div>
                    <div className="form-check form-check-inline">
                        <input onChange={this.handleCheckboxChange}
                               checked={filters.photo}
                               value="photo"
                               className="form-check-input"
                               type="checkbox"
                               id="filter-photo" />
                        <label className="form-check-label" htmlFor="filter-photo">With photo</label>
                    </div>
                    <div className="form-check form-check-inline">
                        <input onChange={this.handleCheckboxChange}
                               checked={filters.link}
                               value="link"
                               className="form-check-input"
                               type="checkbox"
                               id="filter-link" />
                        <label className="form-check-label" htmlFor="filter-link">With link</label>
                    </div>
                    <div className="form-check form-check-inline">
                        <input onChange={this.handleCheckboxChange}
                               checked={filters.isSpecial}
                               value="isSpecial"
                               className="form-check-input"
                               type="checkbox"
                               id="filter-special" />
                        <label className="form-check-label" htmlFor="filter-special">Special news</label>
                    </div>
                </div>
                <div>
                    <strong>Filter by categories: </strong>
                    <Select
                        value={value}
                        isMulti
                        onChange={this.handleSelectChange}
                        options={this.convertCatsToOptions(categories)}
                    />
                </div>
            </div>
        );
    }
}

export default NewsFilters;

NewsFilters.propTypes = {
    setFilter: PropTypes.func,
    filters: PropTypes.shape({
        photo: PropTypes.bool,
        link: PropTypes.bool,
        isSpecial: PropTypes.bool,
        categories: PropTypes.oneOfType([
            PropTypes.array,
            PropTypes.bool
        ]),
    }),
};

NewsFilters.defaultProps = {
    filters: {
        photo: false,
        link: false,
        isSpecial: false,
        categories: false,
    },
};