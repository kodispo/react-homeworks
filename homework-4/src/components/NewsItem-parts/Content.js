import React, {Component} from 'react';
import PropTypes from "prop-types";

class Content extends Component {
    render() {
        const {content} = this.props;
        return (
            <div className="news-item-text" dangerouslySetInnerHTML={{__html: content}}></div>
        );
    }
}

export default Content;

Content.propTypes = {
    content: PropTypes.string,
};

Content.defaultProps = {
    content: 'Learn more...',
};