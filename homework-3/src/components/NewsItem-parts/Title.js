import React, {Component} from 'react';
import PropTypes from "prop-types";

class Title extends Component {
    render() {
        const {title} = this.props;
        return (
            <h3 className="news-item-title">{title}</h3>
        );
    }
}

export default Title;

Title.propTypes = {
    title: PropTypes.string,
};

Title.defaultProps = {
    title: 'News Title',
};