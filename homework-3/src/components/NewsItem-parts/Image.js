import React, {Component} from 'react';
import {LazyLoadImage} from "react-lazy-load-image-component";
import PropTypes from "prop-types";

class Image extends Component {
    render() {
        const {photo} = this.props;
        return (
            <div className="news-item-img">
                {photo && (<LazyLoadImage src={photo} effect="blur"/>)}
            </div>
        );
    }
}

export default Image;

Image.propTypes = {
    photo: PropTypes.string,
};

Image.defaultProps = {
    photo: "https://via.placeholder.com/500x500?text=News+Placeholder",
};