import React, {Component} from 'react';
import PropTypes from "prop-types";

class Author extends Component {
    render() {
        const {author} = this.props;
        return (
            <p className="news-item-author">{author}</p>
        );
    }
}

export default Author;

Author.propTypes = {
    author: PropTypes.string,
};

Author.defaultProps = {
    author: 'News Author',
};