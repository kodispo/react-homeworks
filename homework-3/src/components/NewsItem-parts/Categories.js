import React, {Component} from 'react';
import PropTypes from "prop-types";

class Categories extends Component {
    renderCategories(categoriesArr) {
        return categoriesArr.map((item) => {
            const {id, name} = item;
            return (
                <li key={id}>
                    <span className="badge bg-warning text-dark">{name}</span>
                </li>
            )
        });
    }

    render() {
        const {categories} = this.props;
        return categories && (
            <ul className="news-item-cats">
                {this.renderCategories(categories)}
            </ul>
        );
    }
}

export default Categories;

Categories.propTypes = {
    categories: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
    })),
};

Categories.defaultProps = {
    categories: [
        {
            "id": "0",
            "name": "Uncategorized"
        },
    ],
};