import React, {Component} from 'react';
import {format} from 'date-fns';
import PropTypes from "prop-types";

class NewsDate extends Component {
    render() {
        const {dateCreated} = this.props;
        return (
            <div>
                <p className="news-item-date">{format(dateCreated, 'MMM d, yyyy')}</p>
            </div>
        );
    }
}

export default NewsDate;

NewsDate.propTypes = {
    dateCreated: PropTypes.instanceOf(Date),
};

NewsDate.defaultProps = {
    dateCreated: new Date(),
};