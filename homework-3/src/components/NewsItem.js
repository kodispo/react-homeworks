import React, {Component} from 'react';
import Image from "./NewsItem-parts/Image";
import Title from "./NewsItem-parts/Title";
import Author from "./NewsItem-parts/Author";
import NewsDate from "./NewsItem-parts/NewsDate";
import Categories from "./NewsItem-parts/Categories";
import Content from "./NewsItem-parts/Content";
import PropTypes from "prop-types";

class NewsItem extends Component {
    renderNewsItem(itemObject) {
        const {title, content, dateCreated, categories, photo, author} = itemObject;
        return (
            <>
                <Image photo={photo} />
                <div className="news-item-content">
                    <Title title={title} />
                    <div className="news-item-head">
                        <Author author={author} />
                        <NewsDate dateCreated={new Date(dateCreated)} />
                    </div>
                    <Categories categories={categories} />
                    <Content content={content} />
                </div>
            </>
        )
    }

    render() {
        const {itemObject} = this.props;
        const {link, isSpecial} = itemObject;
        return (
            <div className={'NewsItem col-12' + (isSpecial ? ' featured' : '')}>
                {isSpecial && (<span className="badge bg-primary special-badge">Special</span>)}
                {link ? (
                    <a className="news-item" href={link}>
                        {this.renderNewsItem(itemObject)}
                    </a>
                ) : (
                    <div className="news-item">
                        {this.renderNewsItem(itemObject)}
                    </div>
                )}
            </div>
        );
    }
}

export default NewsItem;

NewsItem.propTypes = {
    itemObject: PropTypes.shape({
        link: PropTypes.string,
        isSpecial: PropTypes.bool,
        title: PropTypes.string,
        content: PropTypes.string,
        dateCreated: PropTypes.string,
        categories: PropTypes.array,
        photo: PropTypes.string,
        author: PropTypes.string,
    }),
};