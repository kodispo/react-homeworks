import React, {Component} from 'react';
import PropTypes from "prop-types";

class NewsFilters extends Component {
    handleChange = (e) => {
        this.props.setFilter({
            [e.target.value]: e.target.checked,
        });
    }

    render() {
        const {filters} = this.props;
        return (
            <div className="NewsFilters mb-5">
                <div className="form-check form-check-inline">
                    <input onChange={this.handleChange}
                           checked={filters.photo}
                           value="photo"
                           className="form-check-input"
                           type="checkbox"
                           id="filter-photo" />
                    <label className="form-check-label" htmlFor="filter-photo">With photo</label>
                </div>
                <div className="form-check form-check-inline">
                    <input onChange={this.handleChange}
                           checked={filters.link}
                           value="link"
                           className="form-check-input"
                           type="checkbox"
                           id="filter-link" />
                    <label className="form-check-label" htmlFor="filter-link">With link</label>
                </div>
                <div className="form-check form-check-inline">
                    <input onChange={this.handleChange}
                           checked={filters.isSpecial}
                           value="isSpecial"
                           className="form-check-input"
                           type="checkbox"
                           id="filter-special" />
                    <label className="form-check-label" htmlFor="filter-special">Special news</label>
                </div>
            </div>
        );
    }
}

export default NewsFilters;

NewsFilters.propTypes = {
    setFilter: PropTypes.func,
    filters: PropTypes.shape({
        photo: PropTypes.bool,
        link: PropTypes.bool,
        isSpecial: PropTypes.bool,
    }),
};

NewsFilters.defaultProps = {
    filters: {
        photo: false,
        link: false,
        isSpecial: false,
    },
};