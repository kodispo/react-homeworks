import React, {Component} from 'react';
import PropTypes from "prop-types";
import NewsItem from "./NewsItem";

class NewsList extends Component {
    render() {
        const {news} = this.props;
        return news && (
            <div className="NewsList row">
                {news.map((itemObject) => (
                    <NewsItem key={itemObject.id} itemObject={itemObject} />
                ))}
            </div>
        );
    }
}

export default NewsList;

NewsList.propTypes = {
    news: PropTypes.arrayOf(PropTypes.shape({
        link: PropTypes.string,
        isSpecial: PropTypes.bool,
        title: PropTypes.string,
        content: PropTypes.string,
        dateCreated: PropTypes.string,
        categories: PropTypes.array,
        photo: PropTypes.string,
        author: PropTypes.string,
    })),
};

NewsList.defaultProps = {
    news: [],
};