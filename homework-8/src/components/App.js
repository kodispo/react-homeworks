import React from "react";
import {Route, Routes} from "react-router-dom";
import Nav from "./Nav";
import NewsPage from "./NewsPage/NewsPage";
import PlanetList from "./PlanetList";

const App = () => {
    return (
        <div className="App">
            <Nav />
            <main className="container">
                <Routes>
                    <Route path="/" element={<NewsPage />} />
                    <Route path="planets" element={<PlanetList />} />
                </Routes>
            </main>
        </div>
    );
};

export default App;
