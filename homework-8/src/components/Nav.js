import React from "react";
import {NavLink} from "react-router-dom";

const Nav = () => {
    return (
        <nav>
            <ul className="nav justify-content-center">
                <li className="nav-item">
                    <NavLink to="/" className="nav-link">News</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink to="/planets" className="nav-link">SW Planets</NavLink>
                </li>
            </ul>
        </nav>
    );
};

export default Nav;
