import React from 'react';
import Select from "react-select";
import hashtagsList from "../../data/hashtags.json";
import authorsList from "../../data/authors.json";
import PropTypes from "prop-types";

const NewsFilter = (props) => {
    const {setFilter, filters} = props;

    const onTextChange     = (e) => setFilter({text: e.target.value});
    const onHashtagsChange = (value) => setFilter({hashtags: value});
    const onAuthorChange   = (value) => setFilter({author: value !== null ? [value] : []});;

    return (
        <form className="NewsForm">
            <h3 className="mb-3">Filter</h3>
            <label className="d-block mb-3">
                <span>Text</span>
                <input value={filters.text} type="text" className="form-control" onChange={onTextChange}/>
            </label>
            <label className="d-block mb-3">
                <span>Hash tags</span>
                <Select value={filters.hashtags} isMulti options={hashtagsList} onChange={onHashtagsChange}/>
            </label>
            <label className="d-block mb-3">
                <span>Author</span>
                <Select value={filters.author} options={authorsList} onChange={onAuthorChange} isClearable/>
            </label>
        </form>
    );
};

NewsFilter.propTypes = {
    setFilter: PropTypes.func,
    filters: PropTypes.shape({
        text: PropTypes.string,
        hashtags: PropTypes.arrayOf(
            PropTypes.shape({
                label: PropTypes.string,
                value: PropTypes.string,
            })
        ),
        author: PropTypes.arrayOf(
            PropTypes.shape({
                label: PropTypes.string,
                value: PropTypes.string,
            })
        ),
    }),
};

NewsFilter.defaultProps = {
    filters: {
        text: '',
        hashtags: [],
        author: [],
    },
};

export default NewsFilter;
