import React from 'react';
import PropTypes from 'prop-types';
import NewsItem from "./NewsItem";

const NewsList = (props) => {
    const {news, onRemoveNews} = props;

    return (
        <div className="NewsList mt-5">
            {news.length ? news.map((item) => (
                <NewsItem key={item.id} item={item} onRemoveNews={onRemoveNews}/>
            )) : (
                <p className="text-secondary fst-italic">No news found.</p>
            )}
        </div>
    );
};

NewsList.propTypes = {
    news: PropTypes.array,
    onRemoveNews: PropTypes.func
};

export default NewsList;
