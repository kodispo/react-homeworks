import React, {useState, useRef} from 'react';
import PropTypes from 'prop-types';
import hashtagsList from '../../data/hashtags.json';
import authorsList from '../../data/authors.json';
import Select from 'react-select';
import faker from 'faker';
import {getBase64} from "../../utils/base64";

const NewsForm = (props) => {
    const {onAddNewsItem} = props;

    const [photo, setPhoto] = useState();
    const [errors, setErrors] = useState({
        title: false,
        description: false,
        photo: false,
        hashtags: false,
        author: false,
    });

    const titleRef       = useRef();
    const excerptRef     = useRef();
    const descriptionRef = useRef();
    const photoRef       = useRef();
    const tagsRef        = useRef();
    const authorRef      = useRef();

    const handleSubmit = (e) => {
        e.preventDefault();

        const newsItem = {
            title: titleRef.current.value,
            excerpt: excerptRef.current.value,
            description: descriptionRef.current.value,
            photo: photo,
            hashtags: tagsRef.current.getValue(),
            author: authorRef.current.getValue(),
        }
        
        if (isDataValid(newsItem)) {
            onAddNewsItem({
                id: faker.datatype.uuid(),
                ...newsItem,
            });
            resetForm();
        }
    };

    const isDataValid = (newsItem) => {
        const newErrors = {};
        let submit = true;

        for (const [key] of Object.entries(errors)) {
            const hasError = !newsItem[key] || (Array.isArray(newsItem[key]) && newsItem[key].length === 0);
            newErrors[key] = hasError;
            if (hasError) submit = false;
        }

        setErrors(newErrors);
        return submit;
    };

    const resetForm = () => {
        titleRef.current.value = '';
        excerptRef.current.value = '';
        descriptionRef.current.value = '';
        photoRef.current.value = '';
        tagsRef.current.setValue([])
        authorRef.current.setValue([])
    };

    const handleChangePhoto = (e) => {
        getBase64(e.currentTarget.files[0], base64 => setPhoto(base64));
    };

    return (
        <form onSubmit={handleSubmit} className="NewsForm mb-5">
            <h3 className="mb-3">Create news</h3>
            <label className="d-block mb-3">
                <span>Title</span>
                <input ref={titleRef} type="text" className="form-control"/>
                {errors.title && (<span className="text-danger error">The field is required</span>)}
            </label>
            <label className="d-block mb-3">
                <span>Excerpt</span>
                <textarea ref={excerptRef} className="form-control" rows="2"></textarea>
            </label>
            <label className="d-block mb-3">
                <span>Description</span>
                <textarea ref={descriptionRef} className="form-control" rows="4"></textarea>
                {errors.description && (<span className="text-danger error">The field is required</span>)}
            </label>
            <label className="d-block mb-3">
                <span>Photo</span>
                <input ref={photoRef} type="file" className="form-control"
                       accept=".jpg,.jpeg,.png" onChange={handleChangePhoto}/>
                {errors.photo && (<span className="text-danger error">The field is required</span>)}
            </label>
            <label className="d-block mb-3">
                <span>Hash tags</span>
                <Select ref={tagsRef} isMulti options={hashtagsList}/>
                {errors.hashtags && (<span className="text-danger error">The field is required</span>)}
            </label>
            <label className="d-block mb-3">
                <span>Author</span>
                <Select ref={authorRef} options={authorsList}/>
                {errors.author && (<span className="text-danger error">The field is required</span>)}
            </label>
            <div className="mt-4">
                <button type="submit" className="btn btn-outline-primary">Add news item</button>
            </div>
        </form>
    );
};

NewsForm.propTypes = {
    onAddNewsItem: PropTypes.func,
};

export default NewsForm;