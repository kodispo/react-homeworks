import React, {useState, useEffect, useCallback} from 'react';
import NewsFilter from "./NewsFilter";
import NewsForm from "./NewsForm";
import NewsList from "./NewsList";

const NewsPage = () => {
    const [news, setNews] = useState([]);
    const [filteredNews, setFilteredNews] = useState([]);
    const [isFormOpen, setIsFormOpen] = useState(false);
    const [filters, setFilters] = useState({
        text: '',
        hashtags: [],
        author: [],
    });

    useEffect(() => {
        let storageNews = localStorage.getItem('newsState');
        storageNews = storageNews ? JSON.parse(storageNews) : null;
        if (storageNews) {
            if (storageNews.news) setNews(storageNews.news);
            if (storageNews.isFormOpen) setIsFormOpen(storageNews.isFormOpen);
            if (storageNews.filters) setFilters(storageNews.filters);
        }
    }, []);

    useEffect(() => {
        localStorage.setItem('newsState', JSON.stringify({news, isFormOpen, filters}));
    }, [news, isFormOpen, filters]);

    const addNewsItem = useCallback((newsItemObj) => {
        setNews([newsItemObj, ...news]);
    }, [news]);

    const removeNews = useCallback((id) => {
        setNews(news.filter((item) => item.id !== id));
    }, [news]);

    const handleCreateNewsClick = () => {
        setIsFormOpen(!isFormOpen);
    }

    const setFilter = useCallback((newFilters) => {
        setFilters({...filters, ...newFilters});
    }, [filters]);

    useEffect(() => {
        let newFilteredNews = news;

        if (filters.text) {
            newFilteredNews = newFilteredNews.filter((newsItem) => {
                return newsItem.title.includes(filters.text)
                    || newsItem.excerpt.includes(filters.text)
                    || newsItem.description.includes(filters.text);
            });
        }
        if (filters.hashtags.length > 0) {
            newFilteredNews = newFilteredNews.filter((newsItem) => {
                const filteredHashtags = filters.hashtags.map(item => item.value);
                const hashtags = newsItem.hashtags.map(item => item.value);
                return filteredHashtags.every((tag) => hashtags.includes(tag));
            });
        }
        if (filters.author.length > 0) {
            newFilteredNews = newFilteredNews.filter((newsItem) => {
                return filters.author[0].value === newsItem.author[0].value;
            });
        }

        setFilteredNews(newFilteredNews);
    }, [news,filters]);

    return (
        <div className="NewsPage container py-5">
            <button type="button" className="btn btn-outline-primary mb-3" onClick={handleCreateNewsClick}>
                {isFormOpen ? 'Hide' : 'Show'} news form
            </button>
            {isFormOpen && (<NewsForm onAddNewsItem={addNewsItem}/>)}
            {news.length > 0 && (<NewsFilter setFilter={setFilter} filters={filters}/>)}
            <NewsList news={filteredNews} onRemoveNews={removeNews}/>
        </div>
    );
};

export default NewsPage;
