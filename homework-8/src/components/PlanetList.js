import React, {useState, useEffect, useMemo} from 'react';
import axios from "axios";

const SW_API_URL = 'https://www.swapi.tech/api/planets';
const LIMIT_PER_PAGE = 10;

const PlanetList = () => {
    const [status, setStatus]   = useState('initial'); // loading|success|error
    const [error, setError]     = useState(null);
    const [planets, setPlanets] = useState([]);

    const [current, setCurrent] = useState(`${SW_API_URL}?page=1&limit=${LIMIT_PER_PAGE}`);
    const [total, setTotal]     = useState(null);
    const [prev, setPrev]       = useState(null);
    const [next, setNext]       = useState(null);

    useEffect(() => {
        setStatus('loading');

        let mountState = {
            isMount: true,
        };

        axios.get(current)
            .then((response) => {
                if (!mountState.isMount) return;

                setStatus('success');
                setError(null);
                setPlanets(response.data.results);

                setTotal(response.data.total_pages);
                setPrev(response.data.previous);
                setNext(response.data.next);
            })
            .catch((error) => {
                if (!mountState.isMount) return;

                console.error(error);
                setStatus('error');
                setError('Failed to load planets. Please try again later');
                setPlanets([]);
            });

        return () => {
            mountState.isMount = false;
        }
    }, [current]);

    const currentPageNumber = useMemo(() => {
        const query = current.substring(current.indexOf('?'));
        return +new URLSearchParams(query).get('page');
    }, [current]);

    const handlePrevClick = (e) => {
        e.preventDefault();
        setCurrent(prev);
    };

    const handlePageClick = (e, key) => {
        e.preventDefault();
        setCurrent(`${SW_API_URL}?page=${key}&limit=${LIMIT_PER_PAGE}`);
    };

    const handleNextClick = (e) => {
        e.preventDefault();
        setCurrent(next);
    };

    return (
        <div className="PlanetList container py-5">
            <h1 className="mb-5 text-center">Planets</h1>
            {status === 'loading' && (<p className="text-primary text-center">Loading planets...</p>)}
            {status === 'error' && (<p className="text-danger text-center">{error}</p>)}
            {status === 'success' && planets.length && (
                <>
                    {planets.map(({uid, name, url}) => (
                        <ul key={uid} className="list-group mb-3">
                            <li className="list-group-item"><strong>UID: </strong> {uid}</li>
                            <li className="list-group-item"><strong>Name: </strong> {name}</li>
                            <li className="list-group-item"><strong>URL: </strong> {url}</li>
                        </ul>
                    ))}
                    <nav>
                        <ul className="nav pagination">
                            {prev && (
                                <li className="nav-item">
                                    <button className="nav-link"
                                            type="button"
                                            onClick={(e) => handlePrevClick(e)}
                                    >Previous page</button>
                                </li>
                            )}
                            {Array.from({length: total}, (_, i) => ++i).map((key) => (
                                <li key={key} className="nav-item">
                                    {key === currentPageNumber ? (
                                        <strong className="nav-link">{key}</strong>
                                    ) : (
                                        <button className="nav-link"
                                                type="button"
                                                onClick={(e) => handlePageClick(e, key)}
                                        >{key}</button>
                                    )}
                                </li>
                            ))}
                            {next && (
                                <li className="nav-item">
                                    <button className="nav-link"
                                            type="button"
                                            onClick={(e) => handleNextClick(e)}
                                    >Next page</button>
                                </li>
                            )}
                        </ul>
                    </nav>
                </>
            )}
        </div>
    );
};

export default PlanetList;
